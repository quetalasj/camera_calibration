import cv2
import numpy as np
import time


def get_perspepective(original_points, target_points):
    # original_points = np.float32([[131, 310], [301, 60], [673, 59], [844, 340]])
    # target_points = np.float32([[0, 1000], [0, 0], [1500, 0], [1500, 1000]])

    M = cv2.getPerspectiveTransform(original_points, target_points)
        

    print(M)
    print("""
    [[  {},   {},  {}],
    [  {},   {},  {}],
    [  {},   {},   {}]]
    """.format(M[0,0], M[0,1], M[0,2], M[1,0], M[1,1], M[1,2], M[2,0], M[2,1], M[2,2]))


def timeit(method):
        def timed(*args, **kw):
            ts = time.time()
            result = method(*args, **kw)
            te = time.time()
            if 'log_time' in kw:
                name = kw.get('log_name', method.__name__.upper())
                kw['log_time'][name] = int(te - ts)
            else:
                print('%r  %2.2f s' % (method.__name__, te - ts))
            return result
        return timed