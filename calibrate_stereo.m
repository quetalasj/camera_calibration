function [stereoParams] = calibrate_stereo(path, squareSize)
% squareSize in units of 'millimeters'

left_path = fullfile(path, 'left');
right_path = fullfile(path, 'right');

left_files = dir(left_path);
right_files = dir(right_path);

[images_num, ~] = size(left_files);

imageFileNames1 = strings(images_num - 2, 1);
imageFileNames2 = strings(images_num - 2, 1);

for i = 1 : images_num - 2
    if left_files(i + 2).name == right_files(i + 2).name
        imageFileNames1(i) = fullfile(left_path, left_files(i + 2).name);
        imageFileNames2(i) = fullfile(right_path, right_files(i + 2).name);
    else
        disp(strcat(left_files(i + 2).name, ' and ', right_files(i + 2).name, " doesn't match"))
    end
end

% Detect checkerboards in images
[imagePoints, boardSize, ~] = detectCheckerboardPoints(imageFileNames1, imageFileNames2);

% Generate world coordinates of the checkerboard keypoints
worldPoints = generateCheckerboardPoints(boardSize, squareSize);

% Read one of the images from the first stereo pair
I1 = imread(imageFileNames1{1});
[mrows, ncols, ~] = size(I1);

% Calibrate the camera
[stereoParams, pairsUsed , ~] = estimateCameraParameters(imagePoints, worldPoints, ...
    'EstimateSkew', false, 'EstimateTangentialDistortion', true, ...
    'NumRadialDistortionCoefficients', 2, 'WorldUnits', 'millimeters', ...
    'InitialIntrinsicMatrix', [], 'InitialRadialDistortion', [], ...
    'ImageSize', [mrows, ncols]);

disp(size(pairsUsed))