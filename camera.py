"""
Contains class for capturing images from camera
"""
import cv2
from calib_tools import timeit

class Camera:

    def __init__(self, camera_num, width=None, height=None, fps=None):
        """
        Args:
            camera_num: 0 (for instance)
            width: 2448 (for instance)
            height: 2048 (for instance)
            fps: 75 (for instance)
        """

        self.cam = cv2.VideoCapture(camera_num)

        if width is not None:
            self.cam.set(cv2.CAP_PROP_FRAME_WIDTH, width)

        if height is not None:
            self.cam.set(cv2.CAP_PROP_FRAME_HEIGHT, height)

        if fps is not None:
            self.cam.set(cv2.CAP_PROP_FPS, fps)
    @timeit
    def read(self):
        _, frame = self.cam.read()
        return _, frame

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self

    def __del__(self):
        self.cam.release()
        del self.cam
        print("Camera released memory")


if __name__ == '__main__':
    """
    test
    """
    cam = Camera(0)
    for i in range(50):
        cv2.imshow('camera.py test', cam.read())
        cv2.waitKey(1)
    del cam
    cv2.destroyAllWindows()

    with Camera(0) as cam:
        for i in range(50):
            cv2.imshow('camera.py test', cam.read())
            cv2.waitKey(1)
    cv2.destroyAllWindows()
