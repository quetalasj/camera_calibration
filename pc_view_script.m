
cam = webcam('3D_SHOP');
cam.Resolution = '2560x960';

% Create a streaming point cloud viewer
player3D = pcplayer([-3, 3], [-3, 3], [0, 8], 'VerticalAxis', 'y', ...
    'VerticalAxisDir', 'down');

while 1
    photo = snapshot(cam);
    [height, width, ~] = size(photo);
    left = photo(:, 1:width/2, :);
    right = photo(:, width/2+1:width,:);

    [frameLeftRect, frameRightRect] = rectifyStereoImages(left, right, stereoParams);

    % Convert to grayscale.
    frameLeftGray  = rgb2gray(frameLeftRect);
    frameRightGray = rgb2gray(frameRightRect);

    % Compute disparity. 
    disparityMap = disparityBM(frameLeftGray, frameRightGray);

    % Reconstruct 3-D scene.
    points3D = reconstructScene(disparityMap, stereoParams);
    points3D = points3D ./ 1000;
    ptCloud = pointCloud(points3D, 'Color', frameLeftRect);
    view(player3D, ptCloud);
end
