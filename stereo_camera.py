"""

"""
import cv2
import numpy as np
from tools.camera import Camera
from pathlib import Path
import matplotlib.pyplot as plt

class StereoCamera(Camera):

    def __init__(self, camera_num, width=None, height=None, fps=None):
        """
        Args:
            camera_num: 0 (for instance)
            width: 2560 (for instance)
            height: 960 (for instance)
            fps: 75 (for instance)
        """
        super(StereoCamera, self).__init__(camera_num, width, height, fps)
        self.calib_path = Path(r"C:\Users\queta\PycharmProjects\Eurobot") / "params_py.xml"


        # cameraMatrix1 = np.array([[537.0195,         0,         304.9085],
        #                 [0.7646,  537.8204,         262.0465],
        #                 [304.9085,  262.0465,    1.0000]])
        # distCoeffs1 = np.array([-0.4249, 0.2410, -8.3826e-05, 0.0010])
        #
        # cameraMatrix2 = np.array([[537.236781101759,         0,         323.6893],
        #                 [0.5560,  538.0930,         250.0762],
        #                 [323.6893,  250.0762,    1.0000]])
        # distCoeffs2 = np.array([-0.4214, 0.2258, 9.0950e-04, -5.5667e-04])
        #
        # R = np.array([[0.999999176135731, -0.000819120612208800, 0.000988316387944370],
        #               [0.000807227134615626, 0.999927970641434, 0.0119750537892967],
        #               [-0.000998054213540012, -0.0119742461276718, 0.999927808052892]])
        # T = np.array([-119.253617128281, -0.245037502017025, 1.30424933913301])
        # E = np.array([[0.000826162938239178, -1.30708973215675, -0.229402409684008],
        #               [1.42210856873811, 1.42912130513621, 119.243706265913],
        #               [0.342720396009614, -119.244829565812, 1.42772760239782]])
        # F = np.array([[2.86358374159882e-09, -4.52379371443889e-06, 0.000757566905548370],
        #               [4.92135944191333e-06, 4.93594445198615e-06, 0.218810709097100],
        #               [-0.000593452073454424, -0.221489651096351, 4.36941028418188]])

        cameraMatrix1 = np.array([[1087.70345294055,	0,	635.252645581191],
                                [0,	1091.00340904135,	541.816265887261],
                                [635.252645581191,	541.816265887261,	1]])
        distCoeffs1 = np.array([-0.416768320335156, 0.168333711229137, -0.00624845470930904, -0.000974616099620546])

        cameraMatrix2 = np.array([[1088.19928892632,	0,	673.727745302293],
                                [0,	1091.85256584210,	518.004526013138],
                                [673.727745302293,	518.004526013138,	1]])
        distCoeffs2 = np.array([-0.412856596464327, 0.159820668070277, -0.00461664938564973, -0.00184832741030164])

        R = np.array([[0.999992162400014,	-8.68036831923943e-05,	-0.00395823239141827],
                        [0.000120398967269656,	0.999963969049116,	0.00848799785743509],
                        [0.00395735298306443,	-0.00848840789899534,	0.999956142182600]])
        T = np.array([-118.537065365788,	-0.743934098943474,	-0.108519353745704])
        E = np.array([[0.00293524416791584,	0.102200932652294,	-0.744822628157134],
                    [-0.577715754928944,	1.00612929123333,	118.531437139432],
                    [0.754217722154159,	-118.532704793710,	1.00913497180034]])
        F = np.array([[2.47984929537921e-09,	8.60835787558176e-08,	-0.000732671064025391],
                    [-4.86451677244685e-07,	8.44624521823491e-07,	0.108411317514053],
                    [0.000943717240658825,	-0.109141102303382,	3.77066180210289]])


        R1, R2, P1, P2, Q, validPixROI1, validPixROI2 = cv2.stereoRectify(cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2, (1280, 960), R, T)
        self.Q = Q
        self.leftMapX, self.leftMapY = cv2.initUndistortRectifyMap(cameraMatrix1, distCoeffs1, R1, P1, (1280, 960), cv2.CV_32FC1)
        self.rightMapX, self.rightMapY = cv2.initUndistortRectifyMap(cameraMatrix2, distCoeffs2, R2, P2, (1280, 960), cv2.CV_32FC1)

        # Setting parameters for StereoSGBM algorithm
        minDisparity = 0
        numDisparities = 16*5#*10
        blockSize = 5
        disp12MaxDiff = 1
        uniquenessRatio = 1
        speckleWindowSize = 1000
        speckleRange = 1000
        window_size = 5  # wsize default 3; 5; 7 for SGBM reduced size image; 15 for SGBM full size image (1300px and above); 5 Works nicely
        # Creating an object of StereoSGBM algorithm
        self.stereo = cv2.StereoSGBM_create(# minDisparity=minDisparity,
                                       # numDisparities=numDisparities,
                                       # blockSize=blockSize,
                                       # disp12MaxDiff=disp12MaxDiff,
                                       # uniquenessRatio=uniquenessRatio,
                                       # speckleWindowSize=speckleWindowSize,
                                       # speckleRange=speckleRange,
                                       #      preFilterCap=75,
                                       # mode = cv2.STEREO_SGBM_MODE_SGBM_3WAY
            minDisparity=-1,
            numDisparities=5 * 16,  # max_disp has to be dividable by 16 f. E. HH 192, 256
            blockSize=window_size,
            P1=8 * 3 * window_size,
            # wsize default 3; 5; 7 for SGBM reduced size image; 15 for SGBM full size image (1300px and above); 5 Works nicely
            P2=32 * 3 * window_size,
            disp12MaxDiff=12,
            uniquenessRatio=10,
            speckleWindowSize=50,
            speckleRange=32*5,
            preFilterCap=63,
            mode=cv2.STEREO_SGBM_MODE_SGBM_3WAY
                                            )
        wsize = 31
        max_disp = 128
        sigma = 2.0
        lmbda = 8000.0
        # Now create DisparityWLSFilter
        self.wls_filter = cv2.ximgproc.createDisparityWLSFilter(self.stereo)
        self.right_matcher = cv2.ximgproc.createRightMatcher(self.stereo);
        self.wls_filter.setLambda(lmbda)
        self.wls_filter.setSigmaColor(sigma)
    def disparity(self, imgL, imgR):
        # Calculating disparith using the StereoSGBM algorithm
        disp = self.stereo.compute(imgL, imgR).astype(np.float32)
        right_disp = self.right_matcher.compute(imgR, imgL).astype(np.float32)
        disp = self.wls_filter.filter(disp, imgL, disparity_map_right=right_disp).astype(np.float32)
        # Calculating disparith using the StereoSGBM algorithm
        disp = cv2.normalize(disp, 0, 255, cv2.NORM_L2)
        return disp

    def read(self):
        frame = super(StereoCamera, self).read()
        frame = cv2.GaussianBlur(frame, (11,11), 0)
        print(frame.shape)
        height, width = frame.shape[:2]
        frame_left = frame[:, 0:width // 2, :]
        frame_right = frame[:, width // 2:, :]
        return frame_left, frame_right

    def read_rectified(self):
        imgL, imgR = self.read()
        left_rectified = cv2.remap(imgL, self.leftMapX, self.leftMapY, cv2.INTER_LINEAR, cv2.BORDER_CONSTANT)
        right_rectified = cv2.remap(imgR, self.rightMapX, self.rightMapY, cv2.INTER_LINEAR, cv2.BORDER_CONSTANT)
        return left_rectified, right_rectified



#Function to create point cloud file
def create_output(vertices, colors, filename):
	colors = colors.reshape(-1,3)
	vertices = np.hstack([vertices.reshape(-1,3),colors])

	ply_header = '''ply
		format ascii 1.0
		element vertex %(vert_num)d
		property float x
		property float y
		property float z
		property uchar red
		property uchar green
		property uchar blue
		end_header
		'''
	with open(filename, 'w') as f:
		f.write(ply_header %dict(vert_num=len(vertices)))
		np.savetxt(f,vertices,'%f %f %f %d %d %d')


import open3d as o3d

def display_inlier_outlier(cloud, ind):
    inlier_cloud = cloud.select_by_index(ind)
    #outlier_cloud = cloud.select_by_index(ind, invert=True)

    print("Showing outliers (red) and inliers (gray): ")
    #outlier_cloud.paint_uniform_color([1, 0, 0])
    #
    o3d.visualization.draw_geometries([inlier_cloud],# outlier_cloud],
                                      zoom=0.3412,
                                      front=[0.4257, -0.2125, -0.8795],
                                      lookat=[2.6172, 2.0475, 1.532],
                                      up=[-0.0694, -0.9768, 0.2024])

if __name__ == "__main__":
    SC = StereoCamera(1, width=1280*2, height=960)
    h, w = (960, 1280)
    f = 0.8 * w  # guess for focal length
    Q = np.float32([[1, 0, 0, -0.5 * w],
                    [0, -1, 0, 0.5 * h],  # turn points 180 deg around x-axis,
                    [0, 0, 0, -f],  # so that y-axis looks up
                    [0, 0, 1, 0]])


    while True:
        im_left, im_right = SC.read_rectified()
        #cv2.imshow('rectified', cv2.resize(cv2.hconcat([im_left, im_right]), (0, 0), fx=0.5, fy=0.5))
        disp = SC.disparity(im_left, im_right)

        cv2.imshow('disparitu', cv2.resize(disp, (0,0), fx=0.5, fy=0.5))

        key = cv2.waitKey(1)
        if key % 256  == 32:
            break
    cv2.destroyAllWindows()
    # Get rid of points with value 0 (i.e no depth)
    mask_map = disp > disp.min()
    d3_image = cv2.reprojectImageTo3D(disp, Q)

    output_points = d3_image[mask_map]
    output_colors = im_left[mask_map]

    # Define name for output file
    output_file = 'reconstructed.ply'
    # Generate point cloud
    print("\n Creating the output file... \n")
    create_output(output_points, output_colors, output_file)

    pcd = o3d.io.read_point_cloud(output_file)

    cl, ind = pcd.remove_radius_outlier(nb_points=10, radius=100)
    o3d.visualization.draw_geometries([pcd],
                                      zoom=0.1,
                                      front=[0.4257, -0.2125, -0.8795],
                                      lookat=[2.6172, 2.0475, 1.532],
                                      up=[-0.0694, -0.9768, 0.2024])

    #display_inlier_outlier(pcd, ind)



