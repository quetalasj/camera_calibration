import cv2
from pathlib import Path
import numpy as np
from tools.capturing import Capturing, process_capture

class StereoCalibration:

    def __init__(self, path, width=None, height=None, camera_name='Stereo Cam'):
        self.path = Path(path)
        self.path_left = self.path / 'left'
        self.path_right = self.path / 'right'

        self.capturing = Capturing(self.path)
        self.capturing_left = Capturing(self.path_left)
        self.capturing_right = Capturing(self.path_right)
        self.camera_name = camera_name

        if width is not None:
            self.width = width
        if height is not None:
            self.height = height

    def detect_chessboard(self, photo):

        gray = cv2.cvtColor(photo, cv2.COLOR_BGR2GRAY)
        ret, corners = cv2.findChessboardCorners(gray, (9, 6), None)


        if ret:
            # Draw and display the corners
            img = cv2.drawChessboardCorners(photo, (9, 6), corners, ret)
            return img

        return photo


    def save_capture(self, photo):
        print(photo.shape)
        height, width = photo.shape[:2]
        photo_left = photo[:, 0:width//2, :]
        photo_right = photo[:, width//2:, :]
        self.capturing_left.save_photo(photo_left)
        self.capturing_right.save_photo(photo_right)

    def proc_func(self, photo):
        height, width = photo.shape[:2]
        photo_left = np.copy(photo[:, 0:width // 2, :])
        photo_right = np.copy(photo[:, width // 2:, :])
        img_left = self.detect_chessboard(photo_left)
        img_right = self.detect_chessboard(photo_right)
        img = cv2.hconcat([img_left, img_right])
        key = self.capturing.imshow(cv2.resize(img, (0,0), fx=0.5, fy=0.5))
        if not self.capturing.process_key(key, space_func=self.save_capture, photo=photo):
            return False
        else:
            return True


    def take_images(self, camera_num):
        process_capture(camera_num, proc_func=self.proc_func, end_func=cv2.destroyAllWindows, width=self.width, height=self.height)


if __name__ == "__main__":
    SC = StereoCalibration(path=r'C:\Users\queta\PycharmProjects\Eurobot\stereo2',
                            width=1280*2, height=960)
    SC.take_images(0)
