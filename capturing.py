import cv2
from pathlib import Path
import os
import datetime

from camera import Camera
from calib_tools import timeit

def process_capture(camera_num, proc_func, end_func=None, **kwargs):
    with Camera(camera_num, **kwargs) as camera:
        true_false = True

        while true_false:
            photo = camera.read()
            true_false = proc_func(photo)
            print('here: ', true_false)

    if end_func is not None:
        end_func()


class Capturing:

    def __init__(self, path_to_save):
        """

        Args:
            path_to_save: path to directory where to save the photos
        """
        self.path_to_save = Path(path_to_save)
        self.photo_counter = 1

        self.photo_name = ''
        self.get_photo_name()

    def get_photo_name(self, suffix='png'):
        files = os.listdir(self.path_to_save)
        self.photo_counter = len(files) + 1
        self.photo_name = self.path_to_save / "photo_{}_{}.{}".format(self.photo_counter, str(datetime.date.today()), suffix)

    def save_photo(self, photo):
        self.get_photo_name()
        cv2.imwrite(str(self.photo_name), photo)

    @timeit
    def imshow(self, photo, scale=(0.5, 0.5)):
        cv2.imshow(str(self.path_to_save), cv2.resize(photo, (0,0), fx=scale[0], fy=scale[1]))
        return cv2.waitKey(1)
    @timeit
    def process_key(self, key, space_func=None, **kwargs):
        if key % 256 == 27:
            # ESC pressed
            print("Escape hit, closing...")
            return False
        elif key % 256 == 32:
            # SPACE pressed
            space_func(**kwargs)
        return True

    def __del__(self):
        cv2.destroyAllWindows()

